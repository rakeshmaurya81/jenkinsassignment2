#!/bin/bash
#Check to installl Nginx
cd /etc
if [ -d nginx ];then
    echo "nginx Already Installed"
else
    echo "Installing Nginx"
    echo
    echo "update the system packages"
    sudo apt-get update
    echo
    sudo apt install NGINX
    sudo systemctl start NGINX
    sudo systemctl status NGINX
    echo "Install Successfully"
fi
#Check to create user
if [ $(getent passwd "JOE") ];then
    echo "User JOE already exist"
else
    sudo useradd JOE
    sudo mkdir /home/JOE
    echo "User JOE created Successfully"
fi
